package com.hexaware.Ajay;
import java.util.*;
public class HashMapDemo
{
 public static void main(String[] args) {
	
	 BankAccount account1=new SavingsAcc("bhagwan ajay",2000,1000L);
	 BankAccount account2=new SavingsAcc("bhagwan syed",3000,1001L);
	 BankAccount account3=new SavingsAcc("bhagwan chama",4000,1002L);
	 BankAccount account4=new SavingsAcc("bhagwan ajay chama",8000,1003L);
	 
	 Map<Long, BankAccount> map =new HashMap<>();
	 map.put(1000L, account1);
	 map.put(1001L, account2);
	 map.put(1002L, account3);
	 map.put(1003L, account4);
	 
	 BankAccount bankaccount=map.get(1002L);
	 System.out.println("Bank account object for id 1002 is "+bankaccount);
 
	 Set<Long> keys=map.keySet();
	 Iterator<Long>it=keys.iterator();
	 while(it.hasNext())
	 {
		 System.out.println(map.get(it.next()));
	 }
	 
	 /* Collection<BankAccount>values =map.values();
	 Iterator<BankAccount> it1=values.iterator();
	 while(it1.hasNext())
	 {
		 BankAccount account=(BankAccount) it1.next();
		 System.out.println(account);
	 }
	 Set<Map.Entry<Long, BankAccount>> entrySet=map.entrySet();
	 Iterator<Map.Entry<Long, BankAccount>> it=entrySet.iterator();
	 
	 while(it.hasNext())
	 {
		 Map.Entry<Long, BankAccount> entry =it.next();
		 System.out.println("Key is "+entry.getKey()+"value is "+entry.getValue());
	 }
}
*/
	 
}
}
