package com.hexaware.Ajay;

public class CurrentAcc extends BankAccount implements Comparable<CurrentAcc>
{
    public CurrentAcc(String name,Double accbal)
    {
   super(name,accbal);
}
     @Override
    public void Deposit(double amount)
    {
        accbal=accbal+amount;
        System.out.println("updated account balance is "+accbal);
    }
    @Override
    public void Withdrawl(double amount)
    {
        if(amount<=25000)
        {
            accbal=accbal-amount;
            System.out.println("Remaining balance is "+accbal);
        }
        else{
            System.out.println("Cannot Process ");
        }
    }
	@Override
	public int compareTo(CurrentAcc b) 
	{
		int result=(int)this.customerID-b.customerID;
		return result;
		
	}
}