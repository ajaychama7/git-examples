package com.hexaware.Ajay;

public abstract class BankAccount
	{
	
	     String name;
	     long customerID;
	     static int id=1000;
	     double accbal;

	    public BankAccount(String name,double accbal,long idu)
	    {
	        this.name=name;
	        this.accbal=accbal;
	        this.customerID=idu;
	    }
	    public BankAccount(String name)
	    {
	        this.name=name;
	        this.customerID=id;
	        id=id+1;
	    }
	    public String getname()
	    {
	        return this.name;
	    }
	    @Override
		public String toString() {
			return "BankAccount [name=" + name + ", customerID=" + customerID + " , accbal=" + accbal
					+ "]";
		}
		public int getcustomerID()
	    {
	        return this.customerID;
	    }
	    public abstract void Deposit(double amount);
	    
	    public abstract void Withdrawl(double amount);
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			long temp;
			temp = Double.doubleToLongBits(accbal);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			result = prime * result + customerID;
			result = prime * result + id;
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			BankAccount other = (BankAccount) obj;
			if (Double.doubleToLongBits(accbal) != Double.doubleToLongBits(other.accbal))
				return false;
			if (customerID != other.customerID)
				return false;
			if (id != other.id)
				return false;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			return true;
		}
	    
	    
	    
	    
	}

