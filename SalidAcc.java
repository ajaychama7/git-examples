package com.hexaware.Ajay;

public class SalidAcc extends BankAccount
{
    public SalidAcc(String name,Double accbal)
    {
   super(name,accbal);
    }
    @Override
    public void Deposit(double amount)
    {
        accbal=accbal+amount;
        System.out.println("Updated account balance is "+accbal);
    }
    @Override
    public void Withdrawl(double amount)
    {
        if(amount<=accbal)
        {
            accbal=accbal-amount;
            System.out.println("Remaining balance is "+accbal);
        }
        else
        {
            System.out.println("cannot process the transaction");
        }
    }
}
